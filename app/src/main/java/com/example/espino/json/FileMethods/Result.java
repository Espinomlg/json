package com.example.espino.json.FileMethods;


public class Result {

    private boolean code; //true es correcto y false indica error
    private String message;
    private String content;

    public boolean getCode() {
        return code;
    }
    public void setCode(boolean code) {
        this.code = code;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }
}
