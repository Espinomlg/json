package com.example.espino.json;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.espino.json.models.Movie;
import com.squareup.picasso.Picasso;

/**
 * Created by espino on 11/01/17.
 */
public class MovieActivity extends AppCompatActivity{

    private TextView title,
            genres,
            duration,
            rating,
            votes,
            plot,
            director,
            actors,
            date,
            country,
            language,
            awards;
    private ImageView image;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);

        init();
    }

    public void init(){
        Movie mov = getIntent().getParcelableExtra(Movie.TAG);

        title = (TextView) findViewById(R.id.movie_title);
        genres = (TextView) findViewById(R.id.movie_genres);
        duration = (TextView) findViewById(R.id.movie_duration);
        rating = (TextView) findViewById(R.id.movie_rating);
        votes = (TextView) findViewById(R.id.movie_votes);
        plot = (TextView) findViewById(R.id.movie_plot);
        director = (TextView) findViewById(R.id.movie_director);
        actors = (TextView) findViewById(R.id.movie_actors);
        date = (TextView) findViewById(R.id.movie_date);
        country = (TextView) findViewById(R.id.movie_country);
        language = (TextView) findViewById(R.id.movie_language);
        awards = (TextView) findViewById(R.id.movie_awards);

        image = (ImageView) findViewById(R.id.movie_img);

        title.setText(mov.getTitle());
        genres.setText(mov.getGenre());
        duration.setText(mov.getDuration());
        rating.setText(String.format(getResources().getString(R.string.movie_rating),mov.getImdbRating()));
        votes.setText(String.format(getResources().getString(R.string.movie_votes),mov.getImdbVotes()));
        plot.setText(mov.getPlot());
        director.setText(String.format(getResources().getString(R.string.movie_director),mov.getDirector()));
        actors.setText(String.format(getResources().getString(R.string.movie_actors),mov.getActors()));
        date.setText(mov.getRelease());
        country.setText(mov.getCountry());
        language.setText(mov.getLanguage());
        awards.setText(mov.getAwards());

        Picasso.with(getApplicationContext())
                .load(mov.getImage())
                .into(image);
    }
}
