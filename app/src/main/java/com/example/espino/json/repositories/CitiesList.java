package com.example.espino.json.repositories;

import com.example.espino.json.models.City;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by espino on 8/01/17.
 */

public class CitiesList {

    private static ArrayList<City> cities;
    private static CitiesList instance;

    public CitiesList(){
        cities = new ArrayList<>();

        cities.add(new City("Málaga", "2514256"));
        cities.add(new City("Barcelona", "6356055"));
        cities.add(new City("Madrid", "6359304"));
        cities.add(new City("Sevilla", "2510911"));
        cities.add(new City("Toledo", "2510409"));
        cities.add(new City("San Sebastian", "3110044"));
        cities.add(new City("Zaragoza", "6362983"));
        cities.add(new City("Pamplona", "6359749"));
        cities.add(new City("Mérida", "2513917"));
    }

    public static CitiesList singleton(){
        if(instance == null)
            instance = new CitiesList();

        return instance;
    }

    public ArrayList<City> getList(){
        return cities;
    }

    public List<String> getCitiesNames(){
        List<String> names = new ArrayList<>();

        for(City c : cities){
            names.add(c.getName());
        }

        return names;
    }
}
