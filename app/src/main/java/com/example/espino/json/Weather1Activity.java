package com.example.espino.json;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.espino.json.models.City;
import com.example.espino.json.repositories.CitiesList;


/**
 * Created by espino on 8/01/17.
 */
public class Weather1Activity  extends AppCompatActivity {

    public static final String TAG = "WEATHER1";

    private ListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather1);

        list = (ListView) findViewById(android.R.id.list);

        ArrayAdapter<String> adapter=new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, CitiesList.singleton().getCitiesNames()){

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view =super.getView(position, convertView, parent);


                TextView textView=(TextView) view.findViewById(android.R.id.text1);
                textView.setTextColor(Color.BLACK);

                return view;
            }
        };

        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bnd = new Bundle();
                bnd.putParcelable(City.TAG, CitiesList.singleton().getList().get(position));
                bnd.putString(NetworkController.ACTION, TAG);
                new NetworkController(Weather1Activity.this, bnd);

            }
        });
    }

}
