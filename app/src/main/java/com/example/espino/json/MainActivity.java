package com.example.espino.json;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void launch(View v){
        Intent i = null;
        switch (v.getId()){
            case R.id.main_btn1:
                i = new Intent(getApplicationContext(), Weather1Activity.class);
                break;
            case R.id.main_btn2:
                i = new Intent(getApplicationContext(), Weather2Activity.class);
                break;
            case R.id.main_btn3:
                i = new Intent(getApplicationContext(), ExchangeActivity.class);
                break;
            case R.id.main_btn4:
                i = new Intent(getApplicationContext(), SearchMovieActivity.class);
                break;
        }

        startActivity(i);
    }
}
