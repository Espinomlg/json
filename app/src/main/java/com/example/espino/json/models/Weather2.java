package com.example.espino.json.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by espino on 9/01/17.
 */

public class Weather2 implements Parcelable{

    public static final String TAG = "WEATHER2";

    private String date,
    tempMin,
    tempMax,
    pressure,
    image;

    protected Weather2(Parcel in) {
        date = in.readString();
        tempMin = in.readString();
        tempMax = in.readString();
        pressure = in.readString();
        image = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(date);
        dest.writeString(tempMin);
        dest.writeString(tempMax);
        dest.writeString(pressure);
        dest.writeString(image);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Weather2> CREATOR = new Creator<Weather2>() {
        @Override
        public Weather2 createFromParcel(Parcel in) {
            return new Weather2(in);
        }

        @Override
        public Weather2[] newArray(int size) {
            return new Weather2[size];
        }
    };

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Weather2(){}

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTempMin() {
        return tempMin;
    }

    public void setTempMin(String tempMin) {
        this.tempMin = tempMin;
    }

    public String getTempMax() {
        return tempMax;
    }

    public void setTempMax(String tempMax) {
        this.tempMax = tempMax;
    }

    public String getPressure() {
        return pressure;
    }

    public void setPressure(String pressure) {
        this.pressure = pressure;
    }
}
