package com.example.espino.json.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.espino.json.R;
import com.example.espino.json.models.Weather2;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by espino on 9/01/17.
 */

public class WeekAdapter extends ArrayAdapter<Weather2> {


    public WeekAdapter(Context context, List<Weather2> objects) {
        super(context, R.layout.listitem_week, objects);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        WeekHolder holder = null;

        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.listitem_week, parent, false);
            holder = new WeekHolder();

            holder.date = (TextView) convertView.findViewById(R.id.listitem_date);
            holder.tempMax = (TextView) convertView.findViewById(R.id.listitem_temp_max);
            holder.tempMin = (TextView) convertView.findViewById(R.id.listitem_temp_min);
            holder.pressure = (TextView) convertView.findViewById(R.id.listitem_pressure);
            holder.image = (ImageView) convertView.findViewById(R.id.listitem_img);

            convertView.setTag(holder);
        }
        else
            holder = (WeekHolder) convertView.getTag();

        holder.date.setText(getItem(position).getDate());
        holder.tempMin.setText(getItem(position).getTempMin());
        holder.tempMax.setText(getItem(position).getTempMax());
        holder.pressure.setText(getItem(position).getPressure());
        Picasso.with(getContext())
                .load(getItem(position).getImage())
                .resize(200,200)
                .into(holder.image);

        return convertView;
    }

    private static class WeekHolder {
        private TextView date,
                tempMax,
                tempMin,
                pressure;
        private ImageView image;
    }
}
