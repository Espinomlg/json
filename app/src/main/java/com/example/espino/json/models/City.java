package com.example.espino.json.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by espino on 8/01/17.
 */

public class City implements Parcelable{

    public static final String TAG = "CITY";

    private String name;
    private String id;

    public City(String name, String id) {
        this.name = name;
        this.id = id;
    }

    protected City(Parcel in) {
        name = in.readString();
        id = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(id);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<City> CREATOR = new Creator<City>() {
        @Override
        public City createFromParcel(Parcel in) {
            return new City(in);
        }

        @Override
        public City[] newArray(int size) {
            return new City[size];
        }
    };

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

}
