package com.example.espino.json;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;

import com.example.espino.json.models.City;
import com.example.espino.json.repositories.CitiesList;

/**
 * Created by espino on 9/01/17.
 */
public class Weather2Activity extends AppCompatActivity{

    public static final String TAG="WEATHER2";
    public static final String CITY_NAME="CITY_NAME";

    private TextInputLayout city;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather2);

        city = (TextInputLayout) findViewById(R.id.weather2_txi);
    }

    public void show(View v){
        if(!TextUtils.isEmpty(city.getEditText().getText().toString())) {

            Bundle bnd = new Bundle();
            bnd.putString(CITY_NAME, city.getEditText().getText().toString());
            bnd.putString(NetworkController.ACTION, TAG);
            new NetworkController(this, bnd);
        }
    }

}
