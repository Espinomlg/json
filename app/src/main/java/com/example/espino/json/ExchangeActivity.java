package com.example.espino.json;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.RadioGroup;

import java.text.DecimalFormat;

/**
 * Created by espino on 10/01/17.
 */

public class ExchangeActivity extends AppCompatActivity {

    public static final String TAG = "EXCHANGE";


    private TextInputLayout dollars, euros;
    private RadioGroup radioGroup;

    private double exchange;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exchange);

        dollars = (TextInputLayout) findViewById(R.id.exchange_txi_dollars);
        euros = (TextInputLayout) findViewById(R.id.exchange_txi_euros);
        radioGroup = (RadioGroup) findViewById(R.id.exchange_radiogroup);

        exchange = -1;
        getNetworkController();

    }

    public void calculate(View v){
        double usd = !TextUtils.isEmpty(dollars.getEditText().getText().toString()) ? Double.valueOf(dollars.getEditText().getText().toString()) : -1;
        double eur = !TextUtils.isEmpty(euros.getEditText().getText().toString()) ? Double.valueOf(euros.getEditText().getText().toString()) : -1;
        DecimalFormat format = new DecimalFormat("#.00");

        switch (radioGroup.getCheckedRadioButtonId()){
            case R.id.exchange_rdbtn_dollarstoeuros:
                if(exchange != -1 && usd != -1){
                    double result = usd * exchange;
                    euros.getEditText().setText(format.format(result));
                }

                break;
            case R.id.exchange_rdbtn_eurostodollars:
                if(exchange != -1 && eur != -1){  double result = eur / exchange;
                    dollars.getEditText().setText(format.format(result));
                }

                break;
        }
    }

    public NetworkController getNetworkController(){
        Bundle bnd = new Bundle();
        bnd.putString(NetworkController.ACTION,TAG);
        return new NetworkController(this, bnd);
    }

    public void setExchange(double exchange){
        this.exchange = exchange;
    }


}
