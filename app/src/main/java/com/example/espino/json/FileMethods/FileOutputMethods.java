package com.example.espino.json.FileMethods;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class FileOutputMethods {

    Context context;

    public FileOutputMethods(Context context){
        this.context = context;
    }

    public boolean writeInternal(String file, String txt, boolean add, String charset){
        File myFile;

        myFile = new File(context.getFilesDir(), file);

        return write(myFile, txt, add, charset);
    }

    private boolean write(File file, String txt, boolean add, String charset){
        FileOutputStream fos = null;
        OutputStreamWriter osw = null;
        BufferedWriter out = null;
        boolean ok;

        try{
            fos = new FileOutputStream(file, add);
            osw = new OutputStreamWriter(fos, charset);
            out = new BufferedWriter(osw);
            out.write(txt);
            ok = true;
        }catch(IOException e){
            Log.e("Error de E/S", e.getMessage());
            ok = false;
        }finally{
            try{
                if(out != null)
                    out.close();
            }catch(IOException e){
                Log.e("Error al cerrar", e.getMessage());
            }
        }

        return ok;
    }

    public String showInternalProperties(String file){

        File myFile = new File(context.getFilesDir(),file);

        return showProperties(myFile);
    }

    private String showProperties(File file){
        SimpleDateFormat dateFormat = null;
        StringBuffer txt = new StringBuffer();

        try{
            if(file.exists()){
                dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss", Locale.getDefault());
                txt.append("Nombre: " + file.getName() + "\n" +
                        "Ruta: " + file.getAbsolutePath() + "\n" +
                        "Tamaño (bytes): " + Long.toString(file.length()) + "\n" +
                        "Ultima modificación: " + dateFormat.format(new Date(file.lastModified()) + "\n"));
            }else
                txt.append("No existe el fichero " + file.getName() + "\n");

        }catch(Exception e){
            Log.e("Error", e.getMessage());
            txt.append(e.getMessage());
        }

        return txt.toString();

    }

    public boolean externalStorageAvailable(){
        boolean exists = false;

        //Comprobamos el estado de la memoria externa (tarjeta SD)
        String state = Environment.getExternalStorageState();

        if (state.equals(Environment.MEDIA_MOUNTED))
            exists = true;

        return exists;
    }

    public boolean writeExternal(String file, String txt, Boolean add, String charset) {
        File myFile, sdcard;
        sdcard = Environment.getExternalStorageDirectory();
        //tarjeta = Environment.getExternalStoragePublicDirectory("datos/programas/");
        //tarjeta.mkdirs();
        myFile = new File(sdcard.getAbsolutePath(), file);

        return write(myFile,txt,add,charset);
    }

}
