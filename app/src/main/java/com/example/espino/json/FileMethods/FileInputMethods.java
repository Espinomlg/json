package com.example.espino.json.FileMethods;


import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class FileInputMethods {

    private Context context;

    public FileInputMethods(Context context){
        this.context = context;
    }

    public Result readInternal(String file, String code) {
        File myfile;
        //mifichero = new File(getApplicationContext().getFilesDir(), nombreFichero);
        myfile = new File(context.getFilesDir(), file);

        return read(myfile, code);
    }

    private Result read(File fichero, String codigo){
        FileInputStream fis = null;
        InputStreamReader isw = null;
        BufferedReader in = null;
        //String linea;
        StringBuilder txt = new StringBuilder();
        Result result= new Result();
        int n;
        result.setCode(true);
        try {
            fis = new FileInputStream(fichero);
            isw = new InputStreamReader(fis, codigo);
            in = new BufferedReader(isw);
            while ((n = in.read()) != -1)
                txt.append((char) n);
        //while ((linea = in.readLine()) != null)
        //txt.append(linea).append('\n');

        } catch (IOException e) {
            Log.e("Error", e.getMessage());
            result.setCode(false);
            result.setMessage(e.getMessage());
        } finally {
            try {
                if (in != null) {
                    in.close();
                    result.setContent(txt.toString());
                }
            } catch (IOException e) {
                Log.e("Error al cerrar", e.getMessage());
                result.setCode(false);
                result.setMessage(e.getMessage());
            }
        }
        return result;
    }

    public Result readExternal(String file, String code){
        File myFile, sdcard;
        //sdcard = Environment.getExternalStoragePublicDirectory("datos/programas/");
        //sdcard.mkdirs();
        sdcard = Environment.getExternalStorageDirectory();
        myFile = new File(sdcard.getAbsolutePath(), file);

        return read(myFile, code);
    }

    public boolean externalStorageAvailable(){
        boolean exists = false;

        //Comprobamos el estado de la memoria externa (tarjeta SD)
        String state = Environment.getExternalStorageState();

        if (state.equals(Environment.MEDIA_MOUNTED))
            exists = true;

        return exists;
    }

}
