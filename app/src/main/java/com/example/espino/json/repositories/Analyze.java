package com.example.espino.json.repositories;

import com.example.espino.json.models.Movie;
import com.example.espino.json.models.Weather;
import com.example.espino.json.models.Weather2;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by espino on 9/01/17.
 */

public class Analyze {

    public static Weather analyzeCityWeather(JSONObject json) throws JSONException {
        Weather weather = new Weather();
        Date date = null;
        JSONObject jsonObject;
        JSONArray jsonContent;
        jsonObject = json.getJSONObject("main");

        weather.setTemp(jsonObject.getString("temp"));
        weather.setPressure(jsonObject.getString("pressure") + " hpa");
        weather.setHumidity(jsonObject.getString("humidity") + "%");

        jsonObject = json.getJSONObject("wind");
        weather.setWindspeed(jsonObject.getString("speed") + "m/s");
        weather.setWindDirection(jsonObject.getString("deg") + "º");

        jsonObject = json.getJSONObject("sys");
        weather.setCountry(jsonObject.getString("country"));
        date = new Date((long)(jsonObject.getDouble("sunrise") * 1000));
        weather.setSunrise(new SimpleDateFormat("hh:mm").format(date));
        date = new Date((long)(jsonObject.getDouble("sunset") * 1000));
        weather.setSunset(new SimpleDateFormat("hh:mm").format(date));

        jsonContent = json.getJSONArray("weather");

        for (int i = 0; i < jsonContent.length(); i++) {
            JSONObject item = jsonContent.getJSONObject(i);
            weather.setCloudiness(item.getString("description"));
            weather.setImg(Weather.getImgBaseurl() + item.getString("icon") + Weather.getImgExtension());


        }
        return weather;
    }
    public static ArrayList<Weather2> analyzeWeekWeatherFromFile(String txt) throws JSONException {
        ArrayList<Weather2> list = new ArrayList<>();
        JSONObject json = new JSONObject(txt);
        Date date = null;
        Weather2 weather = null;
        JSONArray array = json.getJSONArray("list");
        for(int i = 0; i < array.length(); i++){
            JSONObject item = array.getJSONObject(i);
            weather = new Weather2();

            date = new Date(item.getLong("dt") * 1000);
            weather.setDate(new SimpleDateFormat("d-MMM").format(date));
            weather.setTempMax(item.getJSONObject("temp").getString("max") + "ºC");
            weather.setTempMin(item.getJSONObject("temp").getString("min") + "ºC");
            weather.setPressure(item.getString("pressure") + " hpa");

            weather.setImage(Weather.getImgBaseurl() + item.getJSONArray("weather").getJSONObject(0).getString("icon") + Weather.getImgExtension());
            list.add(weather);
        }

        return list;
    }

    public static ArrayList<Weather2> analyzeWeekWeather(JSONObject json) throws  JSONException{
        ArrayList<Weather2> list = new ArrayList<>();

        Date date = null;
        Weather2 weather = null;
        JSONArray array = json.getJSONArray("list");
        for(int i = 0; i < array.length(); i++){
            JSONObject item = array.getJSONObject(i);
            weather = new Weather2();

            date = new Date(item.getLong("dt") * 1000);
            weather.setDate(new SimpleDateFormat("d-MMM").format(date));
            weather.setTempMax(item.getJSONObject("temp").getString("max") + "ºC");
            weather.setTempMin(item.getJSONObject("temp").getString("min") + "ºC");
            weather.setPressure(item.getString("pressure") + " hpa");

            weather.setImage(Weather.getImgBaseurl() + item.getJSONArray("weather").getJSONObject(0).getString("icon") + Weather.getImgExtension());
            list.add(weather);
        }

        return list;
    }

    public static Double analyzeExchange(JSONObject json) throws  JSONException{
        return json.getJSONObject("rates").getDouble("EUR");
    }

    public static Movie analyzeMovie(JSONObject json) throws  JSONException{
        Movie mov = new Movie();

        mov.setTitle(json.getString("Title"));
        mov.setYear(json.getString("Year"));
        mov.setRelease(json.getString("Released"));
        mov.setDuration(json.getString("Runtime"));
        mov.setGenre(json.getString("Genre"));
        mov.setDirector(json.getString("Director"));
        mov.setActors(json.getString("Actors"));
        mov.setPlot(json.getString("Plot"));
        mov.setLanguage(json.getString("Language"));
        mov.setCountry(json.getString("Country"));
        mov.setAwards(json.getString("Awards"));
        mov.setImage(json.getString("Poster"));
        mov.setImdbRating(json.getDouble("imdbRating"));
        String votes = json.getString("imdbVotes").replaceAll(",","");
        mov.setImdbVotes(Integer.valueOf(votes));

        return mov;
    }

}

