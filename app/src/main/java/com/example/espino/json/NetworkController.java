package com.example.espino.json;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.espino.json.FileMethods.FileOutputMethods;
import com.example.espino.json.models.City;
import com.example.espino.json.models.Movie;
import com.example.espino.json.models.Weather;
import com.example.espino.json.models.Weather2;
import com.example.espino.json.repositories.Analyze;
import com.example.espino.json.repositories.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by espino on 9/01/17.
 */

public class NetworkController {

    public static final String ACTION = "Action";
    private static final String BASE_URL_ID = "http://api.openweathermap.org/data/2.5/weather?id=";
    private static final String BASE_URL_NAME = "http://api.openweathermap.org/data/2.5/forecast/daily?q=";//{city name},{country code}&cnt={cnt}"
    private static final String METRIC = "&units=metric";
    private static final String QUANTITY = "&cnt=7";
    private static final String XML = "&mode=xml";
    private static final String COUNTRY_CODE = ",ES";
    private static final String API_KEY = "&appid=8027ea1c6153ad39c7c9c54e9ffe8128";
    private static final String EXCHANGE_URL = "https://openexchangerates.org/api/latest.json?app_id=7819d9031c434557abbfc0f906315f80";
    private static final String MOVIE_URL = "http://www.omdbapi.com/?t=";
    private static final String MOVIE_PLOT = "&plot=full";

    private AppCompatActivity context;
    private Bundle bundle;

    private String url;
    private City city;
    private RequestQueue queue;
    private Weather weather;
    private Movie movie;
    private ArrayList<Weather2> list;
    private double exchange;

    public NetworkController(AppCompatActivity context, Bundle bnd) {
        this.context = context;
        this.bundle = bnd;
        exchange = -1;

        switch (bundle.getString(ACTION)) {
            case Weather1Activity.TAG:
                city = bundle.getParcelable(City.TAG);
                url = BASE_URL_ID + city.getId() + METRIC + API_KEY;
                break;
            case Weather2Activity.TAG:
                url = BASE_URL_NAME + bundle.getString(Weather2Activity.CITY_NAME) + COUNTRY_CODE + METRIC + QUANTITY + API_KEY;
                break;
            case SearchMovieActivity.TAG:
                url = MOVIE_URL + bnd.getString(SearchMovieActivity.MOVIE) + MOVIE_PLOT;
                break;
        }


        queue = VolleySingleton.getInstance(context.getApplicationContext()).getRequestQueue();
        download();


    }

    /*@Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



    }*/


    public void download() {

        switch (bundle.getString(ACTION)) {
            case Weather1Activity.TAG:
                weather1();
                break;
            case Weather2Activity.TAG:
                weather2();
                break;
            case ExchangeActivity.TAG:
                getExchange();
                break;
            case SearchMovieActivity.TAG:
                movieData();
                break;
        }
    }

    public void weather1() {
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Date date = new Date();
                            weather = Analyze.analyzeCityWeather(response);
                            weather.setName(city.getName());
                            weather.setTime("get at " + date.toString().substring(0, 16));
                            Intent i = new Intent(context.getApplicationContext(), CityActivity.class);
                            i.putExtra(Weather.TAG, weather);
                            context.startActivity(i);
                        } catch (JSONException e) {
                            Toast.makeText(context.getApplicationContext(), "Ha ocurrido un error; " + e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context.getApplicationContext(), "Ha ocurrido un error; " + error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });


       // VolleySingleton.getInstance(context.getApplicationContext()).addToRequestQueue(jsObjRequest);

        jsObjRequest.setTag(VolleySingleton.TAG);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 1, 1));
        queue.add(jsObjRequest);
    }

    public void weather2() {
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            FileOutputMethods fom = new FileOutputMethods(context);
                            if(fom.externalStorageAvailable()) {
                                fom.writeExternal("weatherjson", response.toString(), false, "UTF-8");
                            }
                            list = Analyze.analyzeWeekWeather(response);
                            Intent i = new Intent(context.getApplicationContext(), WeekActivity.class);
                            i.putExtra(Weather2.TAG, list);
                            context.startActivity(i);
                        } catch (JSONException e) {
                            Toast.makeText(context.getApplicationContext(), "Ha ocurrido un error; " + e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context.getApplicationContext(), "Ha ocurrido un error; " + error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
        jsObjRequest.setTag(VolleySingleton.TAG);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 1, 1));
        queue.add(jsObjRequest);

        url = BASE_URL_NAME + bundle.getString(Weather2Activity.CITY_NAME) + COUNTRY_CODE + METRIC + XML + QUANTITY + API_KEY;
        StringRequest xmlrequest = new StringRequest
                (Request.Method.GET, url, new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {

                        FileOutputMethods fom = new FileOutputMethods(context);
                        if (fom.externalStorageAvailable()) {
                            fom.writeExternal("weatherxml", response, false, "UTF-8");
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context.getApplicationContext(), "Ha ocurrido un error; " + error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });





        //VolleySingleton.getInstance(context.getApplicationContext()).addToRequestQueue(jsObjRequest);

        xmlrequest.setTag(VolleySingleton.TAG);
        xmlrequest.setRetryPolicy(new DefaultRetryPolicy(3000, 1, 1));
        queue.add(xmlrequest);
    }

    public void getExchange() {
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, EXCHANGE_URL, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            ((ExchangeActivity)context).setExchange(Analyze.analyzeExchange(response));
                        } catch (JSONException e) {
                            Toast.makeText(context.getApplicationContext(), "Ha ocurrido un error; " + e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context.getApplicationContext(), "Ha ocurrido un error; " + error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });


        //VolleySingleton.getInstance(context.getApplicationContext()).addToRequestQueue(jsObjRequest);

        jsObjRequest.setTag(VolleySingleton.TAG);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 1, 1));
        queue.add(jsObjRequest);

    }

    public void movieData(){
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            movie = Analyze.analyzeMovie(response);
                            Intent i = new Intent(context.getApplicationContext(), MovieActivity.class);
                            i.putExtra(Movie.TAG, movie);
                            context.startActivity(i);
                        } catch (JSONException e) {
                            Toast.makeText(context.getApplicationContext(), "Ha ocurrido un error; " + e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context.getApplicationContext(), "Ha ocurrido un error; " + error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });


       // VolleySingleton.getInstance(context.getApplicationContext()).addToRequestQueue(jsObjRequest);

        jsObjRequest.setTag(VolleySingleton.TAG);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 1, 1));
        queue.add(jsObjRequest);
    }

}
