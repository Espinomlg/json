package com.example.espino.json;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import com.example.espino.json.adapters.WeekAdapter;
import com.example.espino.json.models.Weather2;

import java.util.ArrayList;

/**
 * Created by espino on 9/01/17.
 */
public class WeekActivity extends AppCompatActivity{

    private ListView list;
    private WeekAdapter adapter;
    private ArrayList<Weather2> array;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_week);

        list = (ListView) findViewById(android.R.id.list);
        array = getIntent().getParcelableArrayListExtra(Weather2.TAG);

        adapter = new WeekAdapter(getApplicationContext(), array);
        list.setAdapter(adapter);
    }
}
