package com.example.espino.json.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by espino on 9/01/17.
 */

public class Weather implements Parcelable {

    public static final String TAG = "WEATHER";
    private static final String IMG_BASEURL = "http://openweathermap.org/img/w/";
    private static final String IMG_EXTENSION = ".png";

    private String name,
            country,
            img,
            time,
            temp,
            wind,
            windspeed,
            windDirection,
            cloudiness,
            pressure,
            humidity,
            sunrise,
            sunset;

    public Weather(){}

    protected Weather(Parcel in) {
        name = in.readString();
        country = in.readString();
        img = in.readString();
        time = in.readString();
        wind = in.readString();
        windspeed = in.readString();
        windDirection = in.readString();
        cloudiness = in.readString();
        pressure = in.readString();
        humidity = in.readString();
        sunrise = in.readString();
        sunset = in.readString();
        temp = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(country);
        dest.writeString(img);
        dest.writeString(time);
        dest.writeString(wind);
        dest.writeString(windspeed);
        dest.writeString(windDirection);
        dest.writeString(cloudiness);
        dest.writeString(pressure);
        dest.writeString(humidity);
        dest.writeString(sunrise);
        dest.writeString(sunset);
        dest.writeString(temp);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Weather> CREATOR = new Creator<Weather>() {
        @Override
        public Weather createFromParcel(Parcel in) {
            return new Weather(in);
        }

        @Override
        public Weather[] newArray(int size) {
            return new Weather[size];
        }
    };

    public static String getImgExtension() {
        return IMG_EXTENSION;
    }

    public static String getImgBaseurl() {
        return IMG_BASEURL;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getWind() {
        return wind;
    }

    public void setWind(String wind) {
        this.wind = wind;
    }

    public String getWindspeed() {
        return windspeed;
    }

    public void setWindspeed(String windspeed) {
        this.windspeed = windspeed;
    }

    public String getWindDirection() {
        return windDirection;
    }

    public void setWindDirection(String windDirection) {
        this.windDirection = windDirection;
    }

    public String getCloudiness() {
        return cloudiness;
    }

    public void setCloudiness(String cloudiness) {
        this.cloudiness = cloudiness;
    }

    public String getPressure() {
        return pressure;
    }

    public void setPressure(String pressure) {
        this.pressure = pressure;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public String getSunrise() {
        return sunrise;
    }

    public void setSunrise(String sunrise) {
        this.sunrise = sunrise;
    }

    public String getSunset() {
        return sunset;
    }

    public void setSunset(String sunset) {
        this.sunset = sunset;
    }
}
