package com.example.espino.json;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;

/**
 * Created by espino on 11/01/17.
 */

public class SearchMovieActivity extends AppCompatActivity{
    public static final String TAG="SEARCH_MOVIE";
    public static final String MOVIE="MOVIE";

    private TextInputLayout movie;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchmovie);

        movie = (TextInputLayout) findViewById(R.id.searchmovie_txi);
    }

    public void showMovie(View v){
        if(!TextUtils.isEmpty(movie.getEditText().getText().toString())) {

            String name = movie.getEditText().getText().toString();
            name = name.replaceAll("\\s","+");

            Bundle bnd = new Bundle();
            bnd.putString(MOVIE, name);
            bnd.putString(NetworkController.ACTION, TAG);
            new NetworkController(this, bnd);
        }
    }
}
