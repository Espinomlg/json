package com.example.espino.json;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.espino.json.models.City;
import com.example.espino.json.models.Weather;
import com.squareup.picasso.Picasso;

/**
 * Created by espino on 8/01/17.
 */
public class CityActivity extends AppCompatActivity {

    private Weather weather;

    private TextView name,
            temp,
            time,
            description,
            cloudiness,
            wind,
            pressure,
            humidity,
            sunrise,
            sunset;
    private ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city);

        weather = getIntent().getParcelableExtra(Weather.TAG);
        init();

    }

    public void init(){
        name = (TextView) findViewById(R.id.city_txv_name);
        temp = (TextView) findViewById(R.id.city_txv_temp);
        time = (TextView) findViewById(R.id.city_txv_time);
        description = (TextView) findViewById(R.id.city_txv_description);
        cloudiness = (TextView) findViewById(R.id.city_cloudiness);
        wind = (TextView) findViewById(R.id.city_wind);
        pressure = (TextView) findViewById(R.id.city_pressure);
        humidity = (TextView) findViewById(R.id.city_humidity);
        sunrise = (TextView) findViewById(R.id.city_sunrise);
        sunset = (TextView) findViewById(R.id.city_sunset);
        image = (ImageView) findViewById(R.id.city_img_weather);

        name.setText(weather.getName() + "," + weather.getCountry());
        temp.setText(weather.getTemp() + "ºC");
        time.setText(weather.getTime());
        description.setText(weather.getCloudiness());
        cloudiness.setText(weather.getCloudiness());
        wind.setText(weather.getWindspeed() + "\n" + weather.getWindDirection());
        pressure.setText(weather.getPressure());
        humidity.setText(weather.getHumidity());
        sunrise.setText(weather.getSunrise());
        sunset.setText(weather.getSunset());
        Picasso.with(getApplicationContext())
                .load(weather.getImg())
                .resize(200,200)
                .into(image);
    }
}
