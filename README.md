# README #

Entrega de los ejercicicios JSON para la asignatura acceso a datos.

Dependencias gradle utilizadas:
dependencies {
    compile fileTree(include: ['*.jar'], dir: 'libs')
    androidTestCompile('com.android.support.test.espresso:espresso-core:2.2.2', {
        exclude group: 'com.android.support', module: 'support-annotations'
    })
    compile 'com.android.support:appcompat-v7:24.2.1'
    testCompile 'junit:junit:4.12'
    compile 'eu.the4thfloor.volley:com.android.volley:2015.05.28'
    compile 'com.squareup.picasso:picasso:2.5.2'
    compile 'com.android.support:design:24.2.1'
}

Permisos necesarios:
Internet, lectura y escrity

APIS utilizadas:
http://www.openweathermap.org/
https://openexchangerates.org/
https://www.omdbapi.com/ (ejercicio 4)

La API utilizada para el ejercicio 4 es una API no oficial de http://www.imdb.com/(internet movie database, una web de referencia para películas y televisión)
Los datos contienen una amplia información sobre las películas almacenadas y su respectiva puntuación y cantidad de votos en IMDB.
El ejercicio es básicamente un buscador de péliculas. Se introduce el nombre de una peĺicula y mostrará su información si encuentra coincidencia.